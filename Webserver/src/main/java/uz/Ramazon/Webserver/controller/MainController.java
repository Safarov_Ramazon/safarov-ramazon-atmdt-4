package uz.Ramazon.Webserver.controller;
import io.vertx.ext.web.RoutingContext;

public class MainController {

    public void kurs(RoutingContext  routingContext){
        routingContext.response().end("yunalish ATMDT");
    }

    public void stipendiya(RoutingContext  routingContext){
        routingContext.response().end("stipendiya 744`000");
    }

    public void yigindi(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        String son2= routingContext.request().params().get("son2");
        int a1 = Integer.parseInt(son1);
        int a2 = Integer.parseInt(son2);
        int a3=a1+a2;
        routingContext.response().end("yig`indi"+son1+"+"+son2+"="+a3);
    }
    public void ayirma(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        String son2= routingContext.request().params().get("son2");
        int a1 = Integer.parseInt(son1);
        int a2 = Integer.parseInt(son2);
        int a3=a1-a2;
        routingContext.response().end("yig`indi"+son1+"-"+son2+"="+a3);
    }
    public void kopaytma(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        String son2= routingContext.request().params().get("son2");
        int a1 = Integer.parseInt(son1);
        int a2 = Integer.parseInt(son2);
        int a3=a1*a2;
        routingContext.response().end("yig`indi"+son1+"*"+son2+"="+a3);
    }
    public void bolish(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        String son2= routingContext.request().params().get("son2");
        double a1 = Integer.parseInt(son1);
        double a2 = Integer.parseInt(son2);
        double a3=a1/a2;
        routingContext.response().end("yig`indi"+son1+"/"+son2+"="+a3);
    }
    public void conventor(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        double a1 = Integer.parseInt(son1);
        double a2=a1*8300;
        routingContext.response().end(a2+"so`mga teng bo'ladi");
    }
    public void metr(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        double a1 = Integer.parseInt(son1);
        double a2=a1*100;
        routingContext.response().end(a2+"santimetrga teng bo'ladi");
    }
    public void ildiz(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        double a1 = Integer.parseInt(son1);
        double a2=Math.sqrt(a1);
        routingContext.response().end("ildizi"+a2+" teng bo'ladi");
    }
}
