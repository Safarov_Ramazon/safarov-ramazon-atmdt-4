package uz.Ramazon.Webserver.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import uz.Ramazon.Webserver.controller.MainController;

public class MainRouter {
    private MainController mainController;
    public MainRouter(){
        mainController = new MainController();
    }

    public Router getRouter(Vertx vertx){

        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/yunalish").handler(mainController::kurs);
        router.route("/stipendiya").handler(mainController::stipendiya);
        router.route("/yigindi/:son1/:son2").handler(mainController::yigindi);
        router.route("/ayirma/:son1/:son2").handler(mainController::ayirma);
        router.route("/kopaytma/:son1/:son2").handler(mainController::kopaytma);
        router.route("/bolish/:son1/:son2").handler(mainController::bolish);
        router.route("/conventor/:son1").handler(mainController::conventor);
        router.route("/sm/:son1").handler(mainController::metr);
        router.route("/ildiz/:son1").handler(mainController::ildiz);
        router.route().handler(this::page404);
        return router;
    }

    public void page404(RoutingContext  routingContext){

        routingContext
                .response()
                .setStatusCode(404)
                .end("xato 404");
    }
}
